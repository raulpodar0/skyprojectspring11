package com.example.demo.Components;

import com.amadeus.Amadeus;
import com.example.demo.Configs.Keys;

public class AmadeusConnect {

    //INSTANCE;
    public Amadeus amadeus;

    public AmadeusConnect() {
        this.amadeus = Amadeus
                .builder(Keys.AmadeusKey, Keys.AmadeusSecret)
                .build();
    }

}