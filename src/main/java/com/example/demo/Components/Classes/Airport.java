package com.example.demo.Components.Classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Airport {

    private String airportName;
    private String airportIataCode;

    public Airport(String airportName, String airportIataCode) {
        this.airportName = airportName;
        this.airportIataCode = airportIataCode;
    }
}
