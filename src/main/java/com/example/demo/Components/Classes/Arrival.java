package com.example.demo.Components.Classes;

import com.amadeus.resources.FlightOfferSearch;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Arrival extends Departure{

    //private String iataCode;
    private String terminal;
    //private String at;

    public Arrival(FlightOfferSearch.AirportInfo segmentArrival) {
        //this.iataCode = segmentArrival.getIataCode();
        super.iataCode = segmentArrival.getIataCode();
        this.terminal = segmentArrival.getTerminal();
        //this.at = segmentArrival.getAt();
        super.at = segmentArrival.getAt();

    }
}
