package com.example.demo.Components.Classes;

import com.amadeus.resources.FlightOfferSearch;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Departure {

    public String iataCode;
    public String at;

    public Departure() { }

    public Departure(FlightOfferSearch.AirportInfo segmentDeparture) {
        this.iataCode = segmentDeparture.getIataCode();
        this.at = segmentDeparture.getAt();
    }
}
