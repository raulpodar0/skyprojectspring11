package com.example.demo.Components.Classes;

import com.amadeus.resources.FlightOfferSearch;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class FlightInfo {

        private String duration;
        private ArrayList<FlightSegment> flightSegments;
        private String currency;
        private double price;

        public FlightInfo(String duration, ArrayList<FlightSegment> flightSegments, String currency, Double price) {
                this.duration = duration;
                this.flightSegments = flightSegments;
                this.currency = currency;
                this.price = price;
        }

        @Override
        public String toString() {
                return "Flight Duration: " + duration +
                        " Flight Segments: " + flightSegments +
                        " Flight Price: " + price;
        }
}
