package com.example.demo.Components.Classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightOffer {

    private FlightInfo outboundFlight;
    private FlightInfo inboundFlight;

    public FlightOffer(FlightInfo outbound, FlightInfo inbound) {
        this.outboundFlight = outbound;
        this.inboundFlight = inbound;
    }

}
