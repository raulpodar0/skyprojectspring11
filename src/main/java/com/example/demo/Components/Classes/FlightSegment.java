package com.example.demo.Components.Classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightSegment {

    private Departure departure;
    private Arrival arrival;
    private String carrierCode;
    private String number;
    private String duration;

    public FlightSegment(Departure departure, Arrival arrival, String carrierCode, String number, String duration) {
        this.departure = departure;
        this.arrival = arrival;
        this.carrierCode = carrierCode;
        this.number = number;
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Departure: " + departure.iataCode + " at " + departure.at +
                " Arrival: " + arrival.iataCode + " at " + arrival.at +
                " Duration: " + duration;
    }
}
