package com.example.demo.Components.Classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationCoordinates {

    private String locationName;
    private String latitude;
    private String longitude;

    public LocationCoordinates(String locationName, String latitude, String longitude) {
        this.locationName = locationName;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
