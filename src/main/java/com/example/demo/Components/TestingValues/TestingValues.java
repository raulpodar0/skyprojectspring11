package com.example.demo.Components.TestingValues;

public class TestingValues {

    public static String originLocationCode = "EDI";
    public static String destinationLocationCode = "CDG";
    public static String departureDate = "2022-07-08";
    public static String returnDate = "2022-07-16";
    public static int nOfAdults = 1;
    public static int maxResults = 2;
}
