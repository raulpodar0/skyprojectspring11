package com.example.demo;

import com.example.demo.Components.Classes.LocationCoordinates;
import com.example.demo.services.GoogleGeoService;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class ShowDTO {

    private long id;
    private String name;
    private List<LocationCoordinates> locations;
    private String type;


    public ShowDTO (Show show) {
        GoogleGeoService googleGeoService = new GoogleGeoService();
        this.id = show.getId();
        this.name = show.getName();
        this.locations = show.getLocations().stream().map(x->googleGeoService.googleGeocoding(x)).collect(Collectors.toList());
        this.type = show.getType();

    }
}
