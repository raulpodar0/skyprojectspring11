package com.example.demo;

import com.example.demo.Components.AmadeusConnect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class SpringDay2DbStarterApplication  extends SpringBootServletInitializer {

	public static void main(String[] args) {

		SpringApplication.run(SpringDay2DbStarterApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringDay2DbStarterApplication.class);
	}
	@Bean
	public AmadeusConnect amadeusConnect() {
		return new AmadeusConnect();
	}

}