package com.example.demo.controllers;



import com.example.demo.Show;
import com.example.demo.domain.Account;
import com.example.demo.services.AccountService;
import org.springframework.web.bind.annotation.*;



@RestController
public class AccountController {
    private AccountService service;
    public AccountController(AccountService service) {
        this.service = service;
    }

    @PostMapping("/account/create")
    public Account addAccount(@RequestBody Account account) {
        return this.service.addAccount(account);
    }

    @GetMapping("/account/getAccount/{id}")
    public Account getAccount(@PathVariable String id) {
        return this.service.getAccount(id);
    }

    @PutMapping("/account/addShow/{id}")
    public Account addMovie(@PathVariable String id,@RequestParam(name = "movie") Show show){
        return this.service.addShow(id,show);
    }

    @PutMapping("/account/deleteShow/{id}")
    public Account deleteMovie(@PathVariable String id,@RequestParam(name = "movie") Show show){
        return this.service.deleteShow(id,show);
    }
}
