package com.example.demo.controllers;

import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOfferSearch;
import com.amadeus.resources.Location;
import com.example.demo.Components.Classes.Airport;
import com.example.demo.Components.Classes.FlightInfo;
import com.example.demo.Components.Classes.FlightOffer;
import com.example.demo.Components.TestingValues.TestingValues;
import com.example.demo.services.FlightService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
public class FlightController {

    private FlightService flightService;

    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping("API/flight/airports/{latitude}/{longitude}")
    public ArrayList<Airport> getNearbyAirports(@PathVariable String latitude, @PathVariable String longitude) throws ResponseException {
        return flightService.getNearbyAirports(latitude, longitude);
    }

    @GetMapping("API/flight/search/")
    public ArrayList<FlightOffer> flightsOfferSearch() throws ResponseException {
        return flightService.flightsOfferSearch(
                TestingValues.originLocationCode,
                TestingValues.destinationLocationCode,
                TestingValues.departureDate,
                TestingValues.returnDate,
                TestingValues.nOfAdults,
                TestingValues.maxResults);

    }
}