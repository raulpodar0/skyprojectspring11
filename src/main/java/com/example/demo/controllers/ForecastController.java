package com.example.demo.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.example.demo.domain.Forecast;
import com.example.demo.domain.ForecastDTO;
import com.example.demo.services.ForecastService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ForecastController {

    private final ForecastService service;

    public ForecastController(ForecastService service) {
        super();
        this.service = service;
    }

    @GetMapping("/getForecast")
    public List<ForecastDTO> getForecast(@RequestParam(name = "lon") String lon, @RequestParam(name = "lat") String lat) throws JsonProcessingException {
        return this.service.getForecast(lon,lat);
    }
}
