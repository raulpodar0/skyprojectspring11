package com.example.demo.controllers;

import com.example.demo.Components.Classes.LocationCoordinates;
import com.example.demo.services.GoogleGeoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoogleGeoController {

    private GoogleGeoService googleGeoService;

    public GoogleGeoController(GoogleGeoService googleGeoService) {
        this.googleGeoService = googleGeoService;
    }

    @GetMapping("/API/google/geocode/{location}")
    public LocationCoordinates googleGeocoding(@PathVariable String location) {
        return googleGeoService.googleGeocoding(location);
    }
}
