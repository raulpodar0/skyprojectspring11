package com.example.demo.controllers;

import java.util.List;

import com.example.demo.Show;
import com.example.demo.ShowDTO;
import com.example.demo.services.ShowService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.*;


@RestController
public class ShowController {

    private ShowService service;

    public ShowController(ShowService service, RestTemplateBuilder builder) {
        super();
        this.service=service;
    }

    @PostMapping("/create")
    public Show addShow(@RequestBody Show show) {
        return this.service.addShow(show);
    }

    @GetMapping("/getAll")
    public List<ShowDTO> getAll() {
        return this.service.getAllShows();
    }

    @PutMapping("/update/{id}")
    public Show updateShow(@PathVariable("id") Long id, @RequestBody Show show) {
        return this.service.updateShow(id,show);
    }

    @DeleteMapping("/delete/{id}")
    public Boolean removeShow(@PathVariable Long id) {
        return this.service.removeShow(id);
    }


}
