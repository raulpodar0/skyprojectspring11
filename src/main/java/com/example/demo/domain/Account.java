package com.example.demo.domain;

import com.example.demo.Show;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class Account {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    private String name;

    @OneToMany
    private List<Show> shows;
}
