package com.example.demo.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Table(name = "weather")
@Entity
@Getter
@Setter
@ToString
public class Forecast {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private long dt;

    @Column(nullable = false)
    private long sunrise;

    @Column(nullable = false)
    private long sunset;

    @Column(nullable = false)
    private long moonrise;

    @Column(nullable = false)
    private long moonset;

    @Column(nullable = false)
    private double moon_phase;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "forecast", fetch = FetchType.EAGER)
    private Temp temp;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "forecast", fetch = FetchType.EAGER)
    private Feels_Like feels_like;

    @Column
    private int pressure;

    @Column
    private int humidity;

    private double dew_point;

    private double wind_speed;

    private int wind_deg;

    private double wind_gust;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "forecast")
    private List<Weather> weather;

    private int clouds;

    private double pop;

    private double  rain;

    private double uvi;
}