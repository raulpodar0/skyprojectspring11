package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ForecastDTO {

    private long dt;

    private double tempMorn;

    private double tempDay;

    private double tempEve;

    private double tempNight;

    private double tempMin;

    private double tempMax;

    private double pop;

    private String main;

    private String description;

    private String icon;

    public ForecastDTO(Forecast forecast) {

        super();
        this.dt = forecast.getDt();
        this.tempMorn = forecast.getTemp().getMorn();
        this.tempDay = forecast.getTemp().getDay();
        this.tempEve = forecast.getTemp().getEve();
        this.tempNight = forecast.getTemp().getNight();
        this.tempMin = forecast.getTemp().getMin();
        this.tempMax = forecast.getTemp().getMax();
        this.pop = forecast.getPop();
        this.main = forecast.getWeather().get(0).getMain();
        this.description = forecast.getWeather().get(0).getDescription();
        this.icon = forecast.getWeather().get(0).getIcon();

    }

    public ForecastDTO() {

    }
}
