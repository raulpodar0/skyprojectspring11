package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "weather")
@Entity
@Getter
@Setter
@ToString
public class Weather {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "weather_id", nullable = false)
    private Long weather_id;

    @ManyToOne
    @JoinColumn(name = "forecast_id")
    private Forecast forecast;

    private int id;

    private String main;

    private String description;

    private String icon;

    public Long getId() {
        return weather_id;
    }

    public void setId(Long weather_id) {
        this.weather_id = weather_id;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }
}