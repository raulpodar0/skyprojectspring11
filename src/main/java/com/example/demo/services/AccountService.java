package com.example.demo.services;

import com.example.demo.Show;
import com.example.demo.domain.Account;
import com.example.demo.repos.AccountRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private AccountRepo repo;




    public AccountService(AccountRepo repo) {
        super();
        this.repo = repo;
    }

    public Account addAccount(Account account) {
        return this.repo.save(account);
    }

    public Account getAccount(String id) {
        return this.repo.getById(id);
    }

    public Account addShow(String id, Show show) {
        //optional because the id supplied may not be valid
        Optional<Account> existingOptional = Optional.of(this.repo.getById(id));

        //if it's valid store it so we can overwrite it
        Account existingAccount = existingOptional.get();
        List<Show> temp = existingAccount.getShows();
        temp.add(show);
        existingAccount.setShows(temp);

        return this.repo.save(existingAccount);
    }

    public Account deleteShow(String id, Show show) {
        //optional because the id supplied may not be valid
        Optional<Account> existingOptional = Optional.of(this.repo.getById(id));

        //if it's valid store it so we can overwrite it
        Account existingAccount = existingOptional.get();
        List<Show> temp = existingAccount.getShows();
        temp.remove(show);
        existingAccount.setShows(temp);

        return this.repo.save(existingAccount);
    }
}
