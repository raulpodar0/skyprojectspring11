package com.example.demo.services;

import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOfferSearch;
import com.amadeus.resources.Location;
import com.example.demo.Components.AmadeusConnect;
import com.example.demo.Components.Classes.*;
import com.example.demo.Configs.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public class FlightService {

    @Autowired
    private AmadeusConnect amadeusConnect;

    public ArrayList<Airport> getNearbyAirports(String latitude, String longitude) throws ResponseException {

        ArrayList<Airport> nearbyAirports = new ArrayList<>();

        Location[] airports =  amadeusConnect.amadeus.referenceData.locations.airports.get(Params
                    .with("latitude", latitude)
                    .and("longitude", longitude)
                    .and("radius", Constants.RADIUS)
                    .and("sort", "relevance"));

        for (Location a : airports) {
            nearbyAirports.add(new Airport(
                    a.getName(), a.getIataCode()
            ));
        }

        return nearbyAirports;
    }

    public ArrayList<FlightOffer> flightsOfferSearch(String originCode, String destinCode, String dDate, String rDate,
                                        int nAdults, int resultsMax) throws ResponseException {

        FlightOfferSearch[] f = amadeusConnect.amadeus.shopping.flightOffersSearch.get(
                    Params.with("originLocationCode", originCode)
                            .and("destinationLocationCode", destinCode)
                            .and("departureDate", dDate)
                            .and("returnDate", rDate)
                            .and("adults", nAdults)
                            .and("currencyCode", "GBP")
                            .and("max", resultsMax)
                    );

        ArrayList<FlightInfo> flightInfo = new ArrayList<>();
        ArrayList<FlightSegment> flightSegments = new ArrayList<>();
        ArrayList<FlightOffer> flightOffers = new ArrayList<>();
        String flightDuration = null;

        for (FlightOfferSearch flightOfferSearch : f) {

            for (FlightOfferSearch.Itinerary itinerary : flightOfferSearch.getItineraries()) {

                flightDuration = itinerary.getDuration();

                for (FlightOfferSearch.SearchSegment segment : itinerary.getSegments()) {

                    flightSegments.add(new FlightSegment(
                            new Departure(segment.getDeparture()),
                            new Arrival(segment.getArrival()),
                            segment.getCarrierCode(),
                            segment.getNumber(),
                            segment.getDuration())
                    );

                }

                flightInfo.add(new FlightInfo(flightDuration,
                        new ArrayList<>(flightSegments),
                        flightOfferSearch.getPrice().getCurrency(),
                        flightOfferSearch.getPrice().getGrandTotal()
                ));

                flightSegments.clear();

            }

            if (flightInfo.size() > 1)
                flightOffers.add(new FlightOffer(flightInfo.get(0), flightInfo.get(1)));
            else
                flightOffers.add(new FlightOffer(flightInfo.get(0), null));

            flightInfo.clear();

        }

        return flightOffers;
    }

}
