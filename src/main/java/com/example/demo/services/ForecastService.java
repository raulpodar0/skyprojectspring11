package com.example.demo.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.example.demo.domain.Forecast;
import com.example.demo.domain.ForecastDTO;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;



import java.util.ArrayList;
import java.util.List;

@Service
public class ForecastService {

    private List<ForecastDTO> forecastDTOS;
    private ModelMapper mapper;
    private RestTemplate restTemplate;

    public ForecastService(ModelMapper mapper, RestTemplate restTemplate) {
        super();
        this.mapper = mapper;
        this.restTemplate = restTemplate;
    }

    public ForecastDTO mapToDTO(Forecast forecast) {
        return new ForecastDTO(forecast);
    }

    public List<ForecastDTO> getForecast(String lon, String lat) throws JsonProcessingException {
        forecastDTOS = new ArrayList<>();
        String url = "https://api.openweathermap.org/data/2.5/onecall?lat="+lat+"&lon="+lon+"&exclude=alerts,current,minutely,hourly&appid=01ccc21a5f04785393e0a076cf5bbe5f";
        String json=restTemplate.getForObject(url,String.class);
        System.out.println("This that JSON " + json + " for "+ url);
        JsonObject convertedObject = new Gson().fromJson(json, JsonObject.class);

        JsonArray newArray = convertedObject.getAsJsonArray("daily");

        for(int i = 0; i < newArray.size(); i++){
            Forecast forecast = new ObjectMapper().readValue(newArray.get(i).toString(),Forecast.class);
            System.out.println(forecast.toString());
            ForecastDTO temp = this.mapToDTO(forecast);

            forecastDTOS.add(temp);
        }

        return forecastDTOS;
    }
}
