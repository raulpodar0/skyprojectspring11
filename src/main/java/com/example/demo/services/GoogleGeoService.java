package com.example.demo.services;

import com.example.demo.Components.Classes.LocationCoordinates;
import com.example.demo.Configs.Keys;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class GoogleGeoService {

    private static String googleBaseURL = "https://maps.googleapis.com/maps/api/geocode/json?address=";

    public LocationCoordinates googleGeocoding(String location) {

        WebClient.Builder builder = WebClient.builder();
        WebClient webClient = WebClient.create();
        String response =  webClient.get()
                .uri(googleBaseURL + location + "&key=" + Keys.GoogleApiKey)
                .exchange()
                .block()
                .bodyToMono(String.class)
                .block();

        JsonObject jsonResponse = new Gson().fromJson(response, JsonObject.class);
        JsonObject locationInfo = jsonResponse.get("results").getAsJsonArray().get(0).getAsJsonObject();

        return new LocationCoordinates(locationInfo.get("formatted_address").getAsString(),
                locationInfo.get("geometry").getAsJsonObject().get("location").getAsJsonObject().get("lat").getAsString(),
                locationInfo.get("geometry").getAsJsonObject().get("location").getAsJsonObject().get("lng").getAsString());
    }
}
