package com.example.demo.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.demo.Show;
import com.example.demo.ShowDTO;
import com.example.demo.domain.Forecast;
import com.example.demo.domain.ForecastDTO;
import com.example.demo.repos.ShowRepo;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
public class ShowService  {


    private ShowRepo showRepo ;
    private ModelMapper mapper;




    public ShowService(ShowRepo showRepo,ModelMapper mapper) {
        super();
        this.showRepo = showRepo;
        this.mapper = mapper;
    }

    public Show addShow(Show show) {
        return this.showRepo.save(show);
    }
    public ShowDTO mapToDTO(Show show) {
        return new ShowDTO(show);
    }

    public List<ShowDTO> getAllShows() {
        return this.showRepo.findAll().stream().map(x->mapToDTO(x)).collect(Collectors.toList());
    }


    public Show updateShow( Long id,  Show show) {
        //optinal because the id supplied may not be valid
        Optional<Show> existingOptional = Optional.of(this.showRepo.getById(id));

        //if it's valid store it so we can overwrite it
        Show existingShow = existingOptional.get();
        existingShow.setName(show.getName());
        existingShow.setType(show.getType());
        existingShow.setLocations(show.getLocations());
        return this.showRepo.save(existingShow);
    }


    public Boolean removeShow( Long id) {
        this.showRepo.deleteById(id);
        return !this.showRepo.existsById(id);
    }
}
